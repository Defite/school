import { getByPath } from '~/helpers';


const blogStore = {
  actions: {
    async nuxtServerInit ({ commit }) {
      const blogData = await this.$axios.get('/wp-json/wp/v2/posts?_embed');

      const blogPosts = blogData.data.map(function(item) {
        const imageSizes = getByPath('_embedded.wp:featuredmedia.0.media_details.sizes', item) || {}

        return {
          title: item.title.rendered,
          slug: item.slug,
          content: item.content.rendered,
          image: {
            thumbnail: getByPath('medium.source_url', imageSizes) || ''
          }
        }
      });

      commit('setBlogPosts', blogPosts);
    }
  },

  state: {
    posts: null
  },

  mutations: {
    setBlogPosts (state, data) {
      state.posts = data
    }
  }
}

export default blogStore;