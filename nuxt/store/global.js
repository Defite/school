const globalStore = {
    actions: {
        async nuxtServerInit ({ commit }) {
          const headerMenu = await this.$axios.get(`/wp-json/menus/v1/menus/header`);
          const metaData = await this.$axios.get('/wp-json/');
    
          const siteData = {
            title: metaData.data.name,
            desc: metaData.data.description
          };

          const menuItems = headerMenu.data.items.map(function(item) {
            const url = new URL(item.url);
            const path = url.pathname;

            return {
              path,
              title: item.title
            };
          });

          commit('setHeaderMenu', menuItems);

          commit('setSiteData', siteData);
        }
      },
    
      state: {
        header: null,
        page: null,
        site: null,
        post: null
      },
    
      mutations: {
        setPage (state, data) {
          state.page = data
        },
        setHeaderMenu (state, data) {
          state.header = data;
        },
        setSiteData (state, data) {
          state.site = data;
        },
        setPost (state, data) {
          state.post = data;
        }
    }
}

export default globalStore;