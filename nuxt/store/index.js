import Vuex from 'vuex';
import globalStore from './global';
import blogStore from './blog';

const store = () => new Vuex.Store({
  modules: {
    global: globalStore,
    blog: blogStore
  },
})

export default store;